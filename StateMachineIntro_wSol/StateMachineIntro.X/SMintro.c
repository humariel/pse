/* 
 * File:   SMintro.c
 * Author: pf
 *
 * Created on 30 March 2020, 15:16
 */

#include "config_bits.h"
#include <xc.h>

#include <stdio.h>
#include <stdlib.h>

#include "doorLock.h"
#include "timer2.h"
#include "doorLockSM.h"

#define LedPin      PORTAbits.RA3

void max32init(void);

/*
 * 
 */
int main(int argc, char** argv) {
    
    max32init();
    
    
    while(1)
    {
        while (IFS0bits.T2IF == 0);
        IFS0bits.T2IF = 0;

        /* Signal working*/
        if(LedPin == 1)
        {
            LedPin = 0;
        }
        else
        {
            LedPin = 1;
        }
        
        timeTick();
		sm_execute(&smDoorLock);
        
    }

    return (EXIT_SUCCESS);
}

void max32init(void)
{
    int8_t result = 0; 
    
    /* Set RC1 as output*/
    TRISCbits.TRISC1 = 0;
    TRISAbits.TRISA3 = 0;
    
    /* Set RC2 and RC3 as inputs */
    TRISCbits.TRISC2 = 1;
    TRISCbits.TRISC3 = 1;
    
    result = set_timer2_freq(256, 10);

    if (result != 0) {
        LATAbits.LATA3 = 1;
        while (1) {
        }
    }

    set_door_lock(DoorUnlocked);
    sm_init(&smDoorLock);
    timer2control(1);

    
}