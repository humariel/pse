/**
 * doorLock.c
 * 
 * Implementation of door lock system interface for the ChipKit board
 * 
 * pf@ua.pt
 */


#include "doorLock.h"

uint16_t timeElapsed;

/**
* \brief Actuate the door lock, locking and unlocking the door.
*
* \param command Door lock command.
*
*/
void set_door_lock(lock_state_t command)
{
    if(command == DoorLocked)
    {
        DoorLockPin = DoorLockOn;
    }
    else
    {
        DoorLockPin = DoorLockOff;
    }
}

/**
 * \brief Get current door state
 *
 */
door_state_t get_door_state(void)
{
    door_state_t current_door_state;
    
    if(DoorSensorPin == DoorSensorOpen)
    {
        current_door_state = DoorOpened;        
    }
    else
    {
        current_door_state = DoorClosed;
    }
    
    return current_door_state;
}

#if 0
/**
 * \brief Get current door lock state
 *
 */
lock_state_t get_door_lock_state(void)
{
    lock_state_t current_lock_state;
    
    return current_lock_state;
}
#endif

/**
 * \brief Read the pushbutton state
 */
button_state_t read_push_button(void)
{
    button_state_t current_state;
    
    if(PushButtonPin == PushButtonPressed)
    {
        current_state = PB_Pressed;
    }
    else
    {
        current_state = PB_Released;
    }
    return current_state;
}

/**
 * \brief Increment time by a tick
 */
void timeTick(void)
{
    timeElapsed++;
	return;
}

/**
* \brief Get current time
*/
uint16_t get_current_time(void)
{
	return timeElapsed;
}
