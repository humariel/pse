#include "doorLockSM.h"
#include "doorLock.h"


/* Create the machine state data */
sm_t smDoorLock;

#ifdef __XC32__
/* Code is being compiled by XC32 to run in PIC32 */
uint16_t DoorTimeout = 80;
#else
/* Code is being compiled for test, to run on a PC */
#warning Using DoorTimeout values for test
uint16_t DoorTimeout = 4;
#endif

uint16_t UnlockTime;

/*
* State Machine execution
*/
void sm_execute(sm_t *stateMachinep)
{
  switch (stateMachinep->current_state) {
    case LockedClosed:
      set_door_lock(DoorLocked);
      /* Pressing Push Button moves to LockedClosedPB */
      if(read_push_button()==PB_Pressed){
        stateMachinep->current_state = LockedClosedPB;
      }
      break;

    case LockedClosedPB:
    set_door_lock(DoorLocked);

    /* Releasing Push Button moves to UnlockedClosed */
    if(read_push_button()==PB_Released){
      /* Store current time */
      UnlockTime = get_current_time();
      /* Unlock door */
      set_door_lock(DoorUnlocked);
      /* Set next state */
      stateMachinep->current_state = UnlockedClosed;
    }
    break;

    case UnlockedClosed:
      set_door_lock(DoorUnlocked);

      if(get_door_state() == DoorOpened){
        stateMachinep->current_state = UnlockedOpen;
      }
      if(get_current_time() > UnlockTime + DoorTimeout){
        /* TimeOut has elapsed. Lock the door */
        set_door_lock(DoorLocked);
        stateMachinep->current_state = LockedClosed;
      }
    break;

    case UnlockedOpen:
      set_door_lock(DoorUnlocked);

      if(get_door_state() == DoorClosed ){
        /* Lock the door */
        set_door_lock(DoorLocked);
        stateMachinep->current_state = LockedClosed;
      }
    break;

  }
}

void sm_init(sm_t *SMp)
{
  /* The following is a crude way of setting the initial state
     to the first state in the enum */
  SMp->initial_state = (doorLockState_t) 0;
  SMp->current_state = SMp->initial_state;
}
